# Herramientas

## Backend

- GO

## BD

- Mysql

## Frontend

- Node.js 6.9.x or latest
- Angular 6
- Bootstrap 4
- Ionic 3

# Instalacion y Configuaracion

# Clonar Proyecto

- git clone https://gitlab.com/rendall/test.git

## Backend

- Acceder a la carpeta backend : cd test/backend

- Dentro de la carpeta backend instalar las librerias de go utilizadas :

  - go get github.com/go-sql-driver/mysql
  - go get github.com/gorilla/mux
  - go get github.com/rs/cors

- generar un ejecutable del servidor : go build
- ejecutar el servidor.
  - si estas en linux ./backend
  - si estas en windows dar doble clic al ejecutable basta.

## BD

- ejecutar servidor de base de datos.
- Usando cualquier cliente de Mysql, crear una base de datos llamada (test).
- importar la tabla gastos ubicada en test/bd/gastos.sql

## Cliente Web

- Acceder a la carpeta frontend : cd test/frontend
- ejecutar el comando : npm install
- ejecutar el comando : ng serve --open

## Cliente Mobile

- Acceder a la carpeta Mobile : cd test/Mobile
- ejecutar el comando : npm install
- ejecutar el comando : ionic serve
