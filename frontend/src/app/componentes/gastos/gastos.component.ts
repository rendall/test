import { Component, OnInit } from "@angular/core";
import { Gasto } from "../../modelos/gasto";
import { NgForm } from "@angular/forms";
import { GastoService } from "../../servicios/gasto.service";

@Component({
  selector: "app-gastos",
  templateUrl: "./gastos.component.html",
  styleUrls: ["./gastos.component.css"],
  providers: [GastoService]
})
export class GastosComponent implements OnInit {
  constructor(private gastoService: GastoService) {}

  bandera: boolean;
  mensaje: string;
  tipo: string;

  ngOnInit() {
    this.obtenerGastos();
    this.inicializar();
  }

  inicializar() {
    this.bandera = false;
    this.mensaje = "";
    this.tipo = "";
  }

  alert(bandera: boolean, accion: string, obje: any) {
    if (obje == 0) {
      this.bandera = bandera;
      this.mensaje = `${accion}! Rellene Todos los Campos del Formulario`;
      this.tipo = "danger";
    } else {
      let obj = JSON.stringify(obje);
      let mensaje = "",
        tip = "";
      if (obj == '{"status":"OK"}') {
        tip = "success";
        mensaje = `Gasto ${accion} de manera exitosa`;
      } else {
        tip = "danger";
        mensaje = "Ha ocurrido un error";
      }

      this.bandera = bandera;
      this.mensaje = mensaje;
      this.tipo = tip;
    }

    setTimeout(() => {
      if (document.querySelector(".alert")) {
        document.querySelector(".alert").remove();
        this.inicializar();
      }
    }, 7000);
  }

  obtenerGastos() {
    this.gastoService.getGastos().subscribe(res => {
      this.gastoService.gastos = res as Gasto[];
    });
  }

  agregarGasto(form?: NgForm) {
    if (form.value.name && form.value.date && form.value.monto) {
      if (form.value.id) {
        this.gastoService.putGasto(form.value).subscribe(res => {
          this.resetForm(form);
          this.obtenerGastos();
          this.alert(true, "editado", res);
        });
      } else {
        this.gastoService.postGasto(form.value).subscribe(_res => {
          this.resetForm(form);
          this.alert(true, "creado", _res);
          this.obtenerGastos();
        });
      }
    } else {
      this.alert(true, "ERROR", 0);
    }
  }

  editarGasto(gasto: Gasto) {
    this.gastoService.gastoSeleccionado = gasto;
  }

  eliminarGasto(id: string) {
    if (confirm("Estas seguro de eliminar este Gasto?")) {
      this.gastoService.deleteGasto(id).subscribe(_res => {
        this.alert(true, "eliminado", _res);
        this.obtenerGastos();
      });
    }
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.gastoService.gastoSeleccionado = new Gasto();
    }
  }
}
