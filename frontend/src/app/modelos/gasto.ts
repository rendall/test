export class Gasto {
  constructor(id = 0, name = "", date = "", monto = null) {
    this.id = id;
    this.name = name;
    this.date = date;
    this.monto = monto;
  }

  id: number;
  name: string;
  date: string;
  monto: number;
}
