import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Gasto } from "../modelos/gasto";
@Injectable({
  providedIn: "root"
})
export class GastoService {
  gastoSeleccionado: Gasto;
  gastos: Gasto[];

  readonly url_api = "http://localhost:3000/conta/gasto";

  constructor(private http: HttpClient) {
    this.gastoSeleccionado = new Gasto();
  }

  getGastos() {
    return this.http.get(this.url_api + `s`);
  }
  postGasto(gasto: Gasto) {
    return this.http.post(this.url_api, gasto);
  }
  putGasto(gasto: Gasto) {
    return this.http.put(this.url_api + `/${gasto.id}`, gasto);
  }
  deleteGasto(id: string) {
    return this.http.delete(this.url_api + `/${id}`);
  }
}
