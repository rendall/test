// model.go

package main

import (
	"database/sql"
	"fmt"
)

type gasto struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Date  string `json:"date"`
	Monto int    `json:"monto"`
}

func (u *gasto) ObtenerGasto(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT id, name, date, monto FROM gastos WHERE id=%d", u.ID)
	return db.QueryRow(statement).Scan(&u.ID, &u.Name, &u.Date, &u.Monto)
}

func (u *gasto) EditarGasto(db *sql.DB) error {
	statement := fmt.Sprintf("UPDATE gastos SET name='%s', date='%s', monto=%d WHERE id=%d", u.Name, u.Date, u.Monto, u.ID)
	_, err := db.Exec(statement)
	return err
}

func (u *gasto) eliminarGasto(db *sql.DB) error {
	statement := fmt.Sprintf("DELETE FROM gastos WHERE id=%d", u.ID)
	_, err := db.Exec(statement)
	return err
}

func (u *gasto) CrearGasto(db *sql.DB) error {
	statement := fmt.Sprintf("INSERT INTO gastos(name, date, monto) VALUES('%s', '%s', %d)", u.Name, u.Date, u.Monto)
	_, err := db.Exec(statement)

	if err != nil {
		return err
	}

	err = db.QueryRow("SELECT LAST_INSERT_ID()").Scan(&u.ID)

	if err != nil {
		return err
	}

	return nil
}

func listarGastos(db *sql.DB, start, count int) ([]gasto, error) {
	statement := fmt.Sprintf("SELECT id, name, date, monto FROM gastos LIMIT %d OFFSET %d", count, start)
	rows, err := db.Query(statement)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	gastos := []gasto{}

	for rows.Next() {
		var u gasto
		if err := rows.Scan(&u.ID, &u.Name, &u.Date, &u.Monto); err != nil {
			return nil, err
		}
		gastos = append(gastos, u)
	}

	return gastos, nil
}
