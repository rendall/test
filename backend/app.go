package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

type App struct {
	Router *mux.Router
	DB     *sql.DB
}

func (a *App) InicializarApp(usuario, clave, nombre_db string) {
	connectionString := fmt.Sprintf("%s:%s@/%s", usuario, clave, nombre_db)

	var err error
	a.DB, err = sql.Open("mysql", connectionString)
	if err != nil {
		log.Fatal(err)
	}

	a.Router = mux.NewRouter()
	a.InicializarRutas()
}

func (a *App) Run(puerto string) {

	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	a.Router.Use(cors.Handler)

	http.ListenAndServe(puerto, a.Router)
}

func (a *App) InicializarRutas() {
	a.Router.HandleFunc("/conta/gastos", a.getGastos).Methods("GET")
	a.Router.HandleFunc("/conta/gasto", a.createGasto).Methods("POST", "OPTIONS")
	a.Router.HandleFunc("/conta/gasto/{id:[0-9]+}", a.getGasto).Methods("GET")
	a.Router.HandleFunc("/conta/gasto/{id:[0-9]+}", a.updateGasto).Methods("PUT", "OPTIONS")
	a.Router.HandleFunc("/conta/gasto/{id:[0-9]+}", a.deleteGasto).Methods("DELETE", "OPTIONS")
}

func (a *App) getGastos(w http.ResponseWriter, r *http.Request) {

	count, _ := strconv.Atoi(r.FormValue("count"))
	start, _ := strconv.Atoi(r.FormValue("start"))

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	gastos, err := listarGastos(a.DB, start, count)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "error")
		return
	}

	respondWithJSON(w, http.StatusOK, gastos)
}

func (a *App) createGasto(w http.ResponseWriter, r *http.Request) {
	var u gasto
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		respondWithError(w, http.StatusBadRequest, "error")
		return
	}
	defer r.Body.Close()
	if err := u.CrearGasto(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, "error")
		return
	}
	respondWithJSON(w, http.StatusCreated, map[string]string{"status": "OK"})
}

func (a *App) getGasto(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "error")
		return
	}

	u := gasto{ID: id}
	if err := u.ObtenerGasto(a.DB); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "error")
		default:
			respondWithError(w, http.StatusInternalServerError, "error")
		}
		return
	}

	respondWithJSON(w, http.StatusOK, u)
}
func (a *App) updateGasto(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "error")
		return
	}

	var u gasto
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		respondWithError(w, http.StatusBadRequest, "error")
		return
	}
	defer r.Body.Close()
	u.ID = id

	if err := u.EditarGasto(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, "error")
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"status": "OK"})
}
func (a *App) deleteGasto(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "error")
		return
	}
	u := gasto{ID: id}
	if err := u.eliminarGasto(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, "error")
		return
	}
	respondWithJSON(w, http.StatusOK, map[string]string{"status": "OK"})
}
func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"status": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
