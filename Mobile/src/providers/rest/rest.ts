import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
	apiUrl:any;
  
  constructor(public http: HttpClient) {
    //console.log('Hello RestProvider Provider');
    this.apiUrl="http://localhost:3000/conta"
  }

  listarGastos(){
  	return new Promise(resolve =>{
  		this.http.get(this.apiUrl+'/gastos').subscribe(data=>{
  			resolve(data);
  		}, err=>{
  			resolve(err);
  		});
  	});
  }

  agregarGasto(data){
  	return new Promise((resolve, reject)=>{
      data.monto=parseInt(data.monto);
  		this.http.post(this.apiUrl+'/gasto', JSON.stringify(data))
  			.subscribe(res=>{
  				resolve(res);
  			}, (err)=>{
  				reject(err);
  			});
  	});
  }

  editarGasto(data){
    return new Promise((resolve, reject)=>{
      console.log(data);
      data.id=parseInt(data.id);
      data.monto=parseInt(data.monto);
      this.http.put(this.apiUrl+'/gasto/'+data.id, JSON.stringify(data))
        .subscribe(res=>{
          resolve(res);
        }, (err)=>{
          reject(err);
        });
    });
  }


  eliminarGasto(data){
    return new Promise((resolve, reject)=>{
      console.log(data);
      data.id=parseInt(data.id);
      data.monto=parseInt(data.monto);
      this.http.delete(this.apiUrl+'/gasto/'+data.id)
        .subscribe(res=>{
          resolve(res);
        }, (err)=>{
          reject(err);
        });
    });
  }
}
