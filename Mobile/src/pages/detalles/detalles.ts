import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
/**
 * Generated class for the DetallesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalles',
  templateUrl: 'detalles.html',
})
export class DetallesPage {

	gasto:{
		id:number,
		name:string,
		date:string,
		monto:string
	};


  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider : RestProvider) {

  	this.gasto=navParams.data;
  }

  ionViewDidLoad() {
    //console.log(this.gasto);
  }
  regreso(){
  	this.navCtrl.pop();
  }

  editarGasto(gasto){
  	this.restProvider.editarGasto(gasto)
  		.then(data=>{
  			console.log(data);
  			this.regreso();
  		})
  }


}
