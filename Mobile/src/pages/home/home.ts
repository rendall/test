import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import {DetallesPage} from '../detalles/detalles';
import  {AgregarPage} from '../agregar/agregar';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
	
 

export class HomePage {

	gastos:any;
	
	gasto:{
		id:number,
		name:string,
		date:string,
		monto:string
	};

  constructor(public navCtrl: NavController, public restProvider:RestProvider, public navParams: NavParams) {
  	this.listarGastos();
  }

  ionViewWillEnter() {
    this.listarGastos();
  }

  listarGastos(){
  	this.restProvider.listarGastos()
  		.then(data=>{
  			this.gastos=data;
  		});
  }

  inicializarGasto(gasto){
  	this.gasto=gasto;
  	console.log(this.gasto);
  	this.irGasto();
  }

  irGasto(){
  	this.navCtrl.push(DetallesPage, this.gasto);
  }

 eliminarGasto(gasto){
  	this.restProvider.eliminarGasto(gasto)
  		.then(data=>{
  			console.log(data);
  			this.listarGastos();
  		})
  }

 irAgregar(){
 	this.navCtrl.push(AgregarPage);
 }

}
