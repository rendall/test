import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
/**
 * Generated class for the AgregarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agregar',
  templateUrl: 'agregar.html',
})
export class AgregarPage {

	gasto:{
		name:string,
		date:string,
		monto:string
	};

  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider: RestProvider) {
  	this.inicializar();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgregarPage');
  }

  inicializar(){
  	this.gasto={name:"", date: "", monto: ""};
  }

  regreso(){
  	this.navCtrl.pop();
  }
  agregarGasto(gasto){
  	this.restProvider.agregarGasto(gasto)
  		.then(data=>{
  			console.log(data);
  			this.regreso();
  		})
  }

}
